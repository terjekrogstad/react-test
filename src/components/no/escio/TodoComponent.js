'use strict';

import React from 'react';

require('styles/no/escio/Todo.css');

class TodoComponent extends React.Component {
  render() {
  	this.handleClick = ( event )=>{
			window.console.log( 'Click event', event );
		};
  	if( !this.props.title ){
  		return null;
  	}
    return (
      <div className="todo-component">
      	<input type="checkbox" /> {this.props.title} <button onClick={()=>{this.props.removeTodo(this.props.index)}}>x</button>
      </div>
    );
  }
}

TodoComponent.displayName = 'NoEscioTodoComponent';

// Uncomment properties you need
// TodoComponent.propTypes = {};
// TodoComponent.defaultProps = {};

export default TodoComponent;
