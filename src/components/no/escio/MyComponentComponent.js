'use strict';

import React from 'react';

require('styles/no/escio/MyComponent.css');

class MyComponentComponent extends React.Component {
  render() {
    return (
      <div className="mycomponent-component">
        Look at me injected globally and locally
      </div>
    );
  }
}

MyComponentComponent.displayName = 'NoEscioMyComponentComponent';

// Uncomment properties you need
// MyComponentComponent.propTypes = {};
// MyComponentComponent.defaultProps = {};

export default MyComponentComponent;
