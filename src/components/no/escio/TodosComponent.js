'use strict';

import React from 'react';
import TodoComponent from './TodoComponent';

require('styles/no/escio/Todos.css');

//const TodoComponent = require('./TodoComponent');



class TodosComponent extends React.Component {
  render() {
  	if( !this.props.todos ){
  		return null;
  	}

  	let rows = [];

  	this.props.todos.map( (todo,index)=>{

  		rows.push(<TodoComponent title={todo.title} index={index} removeTodo={this.props.removeTodo}/>);
  	});

    return (
      <div className="todos-component">
      	<h1>Todo list</h1>
      	{rows}
      	<hr/>
      	<p>
      		<input type="text"/> <button onClick={()=>{this.props.addTodo('Hello todo')}}>Add</button>
      	</p>
      </div>
    );
  }
}

TodosComponent.displayName = 'NoEscioTodosComponent';

// Uncomment properties you need
// TodosComponent.propTypes = {};
// TodosComponent.defaultProps = {};

export default TodosComponent;
