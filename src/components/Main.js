require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';

//import MyComponent from './no/escio/MyComponentComponent';
import TodosComponent from './no/escio/TodosComponent';

let yeomanImage = require('../images/yeoman.png');

class AppComponent extends React.Component {
  render() {
    let todos = require('json!./todos.json').todos;
    this.removeTodo = (index) => {
      todos.splice(index,1);
      this.setState( {todos : todos} );
    };
    this.addTodo = (title) => {
      todos.push({
        title : title
      });
      this.setState( {todos : todos} )
    };
    return (
      <div className="index">
        <img src={yeomanImage} alt="Yeoman Generator" />
        <div className="notice">
        	Please edit the aaawesome file <code>src/components/Main.js</code> to get started!
        	<TodosComponent todos={todos} removeTodo={this.removeTodo} addTodo={this.addTodo}/>
        </div>
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
